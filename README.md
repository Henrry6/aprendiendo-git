# Apreno Git
Este proyecto es para aprender git y gitLab.
Uso de comandos basicos


```js
//esto es codigo
console.log("Hola mundo")

```

# Manejo de Ramas

```
git branch nombre de rama (crear)
git checkout nombre de rama (cambiar )

git checkout -b nombre de rama (crea y cambia)

```
# Generar clave SSH
La clave ssh sirve para auntenticarse con el gitlab sin usuario y contraseña.

```bash
#generar clave ssh
$ ssh-keygen

# se mostro el contenido de la clave publica
cd ~/.ssh
ls
$ cat id_rsa.pub

#se probo la conexion
ssh -T git@gitlab.com


```